from datetime import datetime

import numpy as np
import pytest

from sbbarrivals import daylight_brightness


@pytest.mark.parametrize(
    "timestamp, expected",
    [
        (datetime(2023, 1, 1, 0, 0, 0), 0),
        (datetime(2023, 1, 1, 12, 0, 0), 1),
        (datetime(2023, 1, 1, 6, 0, 0), np.sin(np.pi / 4) ** 2),
        (datetime(2023, 1, 1, 18, 0, 0), np.sin(np.pi * 3 / 4) ** 2),
    ],
)
def test_daylight_brightness(timestamp, expected):
    b = daylight_brightness(timestamp)

    assert b == expected
